'use strict';

module.exports = function (sequelize, DataTypes) {
    let Department = sequelize.define('Department',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            name: DataTypes.STRING,
            description: DataTypes.STRING
        },
        {
            classMethods: {
                associate: function (models) {
                    Department.hasMany(models.Employee, {as:'Employees',hooks:true});
                }
            }
        }
    );
    Department.belongsTo(Department, {foreignKey: 'parentId', allowNull: true, defaultValue: null});
    return Department;
};