/**
 * Created by 1 on 01.06.2017.
 */
'use strict';

const fs = require('fs'),
    path = require('path'),
    Sequelize = require('sequelize'),
    basename = path.basename(module.filename),
    config = require('../config/config'),
    walkSync = require('../walksync/walksync');


let db = {};
var rootPath =  path.join(__dirname, '..');
let folder = path.join(rootPath, 'db');
try {
    let stat = fs.lstatSync(folder);
    if(!stat.isDirectory()){
        try {
            fs.mkdirSync(folder);
        } catch (ex){
            console.log(ex.stack);
        }
    }
} catch (e) {
    if (e.code === 'ENOENT'){
        try {
            fs.mkdirSync(folder);
        } catch (ex){
            console.log(ex.stack);
        }
    }
}

let sequelize = new Sequelize('test', config.username, config.password, {
    dialect: config.dialect,
    storage: path.join(rootPath, 'db', config.database)
});

let files = walkSync(__dirname, []);
files
    .filter(function (file) {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(function (file) {
        let model = sequelize['import'](file);
        db[model.name] = model;
    });

Object.keys(db).forEach(function (modelName) {
    if(db[modelName].associate){
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
