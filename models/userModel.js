
'use strict';

module.exports = function (sequelize, DataTypes) {
    let Employee = sequelize.define('Employee',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            fullName: DataTypes.STRING,
            sex: DataTypes.STRING,
            birthday: {
                type: DataTypes.DATEONLY
            }
        },
        {
            classMethods: {
                associate: function (models) {
                    //console.log(models.Department);
                    Employee.belongsTo(models.Department, {as:'Department',foreignKey: 'DepartmentId',hooks:true});
                }
            }
        }
    );
    return Employee;
};