var express = require('express');
var router = express.Router();
const controllerDepartment = require('../controller/controllerDepartment'),
    controllerUser = require('../controller/controllerUser');

router.get('/', controllerDepartment.Index);
router.post('/add_dep', controllerDepartment.Add);
router.post('/update_dep', controllerDepartment.Update);
router.get('/delete_department/:id', controllerDepartment.Delete);
router.get('/get_department', controllerDepartment.AddForm);
router.get('/edit_department', controllerDepartment.Edit);

router.post('/employee/add_user', controllerUser.Add);
router.get('/employee/delete_user/:id', controllerUser.Delete);
router.get('/employee/get_user', controllerUser.AddForm);
router.get('/employee/edit_user', controllerUser.Edit);
router.post('/employee/update_user', controllerUser.Update);
router.get('/employee/:id', controllerUser.GetUsers);

module.exports = router;
