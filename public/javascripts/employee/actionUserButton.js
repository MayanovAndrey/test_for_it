$.validator.addMethod(
    "australianDate",
    function(value, element) {
        return value.match(/^\d\d?\.\d\d?\.\d\d\d\d$/);
    },
    "Please enter a date in the format dd.mm.yyyy."
);

function editEmployee(userId) {
    var url;
    var parentIdArr = window.location.href.split('/');
    var parentId = parentIdArr[parentIdArr.length - 1];
    if(userId == null){
        url = 'get_user';
    } else {
        url = 'edit_user';
    }
    $.ajax({
        url: url,
        dataType: 'html',
        data:{
            userId: userId,
            parentId: parentId
        },
        success: function (responce) {

            bootbox.dialog({
                message: responce
            }).on("shown.bs.modal", function () {
                $(function () {
                    $("#datepicker1").datepicker({dateFormat: "dd.mm.yy"})
                });
                $(function () {
                    $('#userFormEdit').validate({
                        rules: {
                            fullName: {
                                required: true,
                                minlength: 2,
                                maxlength: 255
                            },
                            bd: {
                                required: true,
                                australianDate : true
                            }
                        },
                        messages: {
                            fullName: {
                                required: "Введите имя",
                                minlength: "Минимальная длина 2"
                            },
                            bd: {
                                required: "Выберите День рождения"
                            }
                        }
                    });
                });
            });
        }
    });
}

function deleteEmployee(userId) {
        $.ajax({
            url: 'delete_user/' + userId,
            dataType: 'html',
            success: function (responce) {
                if (responce === 'ok') {
                    window.location.replace(window.location.href);
                }
            }
        });
};