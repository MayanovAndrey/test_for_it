$.validator.addMethod(
    "australianDate",
    function(value, element) {
        return value.match(/^\d\d?\.\d\d?\.\d\d\d\d$/);
    },
    "Please enter a date in the format dd.mm.yyyy."
);

$('#userForm').validate({
    rules: {
        fullName: {
            required: true,
            minlength: 2,
            maxlength: 255
        },
        bd: {
            required: true,
            australianDate : true
        }
    },
    messages: {
        fullName: {
            required: "Введите имя",
            minlength: "Минимальная длина 2"
        },
        bd: {
            required: "Выберите День рождения"
        }
    }
});