$('#departmentForm').validate({
    rules: {
        name: {
            required: true,
            minlength: 2,
            maxlength: 255
        },
        desc: {
            required: true,
            minlength: 1,
            maxlength: 255
        }
    },
    messages: {
        name: {
            required: "Введите название",
            minlength: "Минимальная длина 2"
        },
        desc: {
            required: "Введите описание",
            minlength: "Минимальная длина 1"
        }
    }
});