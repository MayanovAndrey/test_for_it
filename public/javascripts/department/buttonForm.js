function editDepartment(idElemnt) {
    var url;
    if (idElemnt == null){
        url = 'get_department';
    } else {
        url = 'edit_department';
    }
    $.ajax({
        url: url,
        type: 'GET',
        data:{
            idElemnt: idElemnt
        },
        dataType: 'html',
        success: function(responce){
            bootbox.dialog({
                message: responce
            }).on("shown.bs.modal", function () {
                $(function () {
                    $('#departmentFormEdit').validate({
                        rules: {
                            name: {
                                required: true,
                                minlength: 2,
                                maxlength: 255
                            },
                            desc: {
                                required: true,
                                minlength: 1,
                                maxlength: 255
                            }
                        },
                        messages: {
                            name: {
                                required: "Введите название",
                                minlength: "Минимальная длина 2"
                            },
                            desc: {
                                required: "Введите описание",
                                minlength: "Минимальная длина 1"
                            }
                        }
                    });
                });
            });
        }
    });
}

function deleteDepartment(idElemnt) {
    $.ajax({
        url: 'delete_department/'+idElemnt,
        dataType: 'html',
        success: function(responce){
            if(responce === 'ok') {
                window.location.replace(window.location.href);
            } else {
                alert('Есть входящие подразделения, удалять нельзя!');
            }
        }
    });
};