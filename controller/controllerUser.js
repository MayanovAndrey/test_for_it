'use strict';
const models = require('../models/index'),
    Promise= require('sequelize').Promise;

exports.GetUsers = (req, res, next) => {
    const parentId = req.params.id;
    models.Employee.findAll({where: {DepartmentId: parentId}})
        .then(employees => {
            res.render('employee/employee', {title: 'Employee',users: employees, parentId: parentId});
            res.end();
        }).catch(err => {
            res.status(500);
            next(err);
        });
};

exports.Add = (req, res, next) => {
    models.Employee.create({fullName: req.body.fullName, sex: req.body.sex, birthday: req.body.bd, DepartmentId: req.body.depId}).then(employee => {
        res.redirect('/employee/' + employee.DepartmentId);
    }).catch(err => {
        res.status(500);
        next(err);
    });
};

exports.Delete = (req, res, next) => {
    const userId = req.params.id;
    models.Employee.findById(userId).then(user => {
        user.destroy().then(() => {
            res.send('ok');
        });
    }).catch(err => {
        res.status(500);
        next(err);
    });
};

exports.AddForm = (req, res) => {
    res.render('employee/modalForEditUser', {parentId: req.query.parentId});
    res.end();
};

exports.Edit = (req, res) => {
    const userId = req.query.userId;
    Promise.join(
        models.Employee.findById(userId),
        models.Department.all(),
        (user, departments) => {
            res.render('employee/modalForEditUser', {user, departments});
            res.end();
        }
    );
};

exports.Update = (req, res, next) => {
    var idPage = req.body.departmentId;
    models.Employee.update({
        fullName: req.body.fullName,
        sex: req.body.sex,
        birthday: req.body.bd,
        DepartmentId: req.body.department
    }, {where: {id: req.body.userId}}).then(user => {
        res.redirect('/employee/' + idPage);
    }).catch(err => {
        res.status(500);
        next(err);
    });
};