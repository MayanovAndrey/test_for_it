'use strict';
const models = require('../models/index'),
    Promise = require('sequelize').Promise;

exports.Index = (req, res, next) => {
    models.Department.findAll()
        .then(departments => {
            var lists = sortDepartmentForMainPage(departments);
            res.render('department/index', {title: 'Main page', departments: lists});
            res.end();
        }).catch(err => {
        res.status(500);
        next(err);
    });
};

function recursiveSortDepartmentForMainPage(parentID, departments, listDepartment) {
    departments.forEach(function (departmet) {
        if(departmet.parentId == parentID) {
            listDepartment.push(departmet);
            recursiveSortDepartmentForMainPage(departmet.id, departments, listDepartment)
        }
    });
}

function sortDepartmentForMainPage(departments) {
    var listDepartment = [];
    departments.forEach(function (departmet) {
        if(departmet.parentId == null) {
            listDepartment.push(departmet);
            recursiveSortDepartmentForMainPage(departmet.id, departments, listDepartment)
        }
    });
    return listDepartment;
}

exports.Add = (req, res, next) => {
    var parentId = null;
    if (!(req.body.parent === 'null')) {
        parentId = req.body.parent;
    }
    models.Department.create({name: req.body.name, description: req.body.desc, parentId: parentId}).then(department => {
        res.redirect('/');
    }).catch(err => {
        res.status(500);
        next(err);
    });
};

exports.Update = (req, res, next) => {
    var parentId = null;
    if (!(req.body.parent === 'null')) {
        parentId = req.body.parent;
    }
    models.Department.update({
        name: req.body.name,
        description: req.body.desc,
        parentId: parentId
    }, {where: {id: req.body.elId}}).then(department => {
        res.redirect('/');
    }).catch(err => {
        res.status(500);
        next(err);
    });
};

exports.Delete = (req, res, next) => {
    const parentId = req.params.id;
    models.Department.findAll({where: {parentId: parentId}}).then(departs => {
        if (departs.length == 0) {
            models.Department.findById(parentId).then(depart => {
                depart.destroy();
                res.send('ok');
            }).catch(err => {
                res.status(500);
                next(err);
            });
        } else {
            res.send('Error');
        }
    });
};

function recursiveSearchDepartmentForEdit(id, parentID, departments, listForEditDepartment) {
    departments.forEach(function (departmet) {
        if(departmet.parentId == parentID && departmet.id != id) {
            listForEditDepartment[departmet.id] = departmet.name;
            recursiveSearchDepartmentForEdit(id, departmet.id, departments, listForEditDepartment)
        }
    });
}

function searchByParentNullForEdit(id, departments) {
    var listForEditDepartment = {};
    departments.forEach(function (departmet) {
        if(departmet.parentId == null && departmet.id != id) {
            listForEditDepartment[departmet.id] = departmet.name;
            recursiveSearchDepartmentForEdit(id, departmet.id, departments, listForEditDepartment)
        }
    });
    return listForEditDepartment;
}

exports.AddForm = (req, res, next) => {
    models.Department.all().then(departments => {
        res.render('department/modalForEditDepartment', {departments});
        res.end();
    }).catch(err => {
        res.status(500);
        next(err);
    });
};

exports.Edit = (req, res) => {
    const parentId = req.query.idElemnt;
    Promise.join(
        models.Department.findById(parentId),
        models.Department.all(),
        (department, departments) => {
            var lists = searchByParentNullForEdit(parentId, departments);
            res.render('department/modalForEditDepartment', {department, lists});
            res.end();
        }
    );
};